cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -G Ninja
ninja -C build
./build/camera_calibration config/top.xml
./build/camera_calibration config/bottom.xml